<?php
date_default_timezone_set("America/Costa_Rica");
    //echo date("l" );//imprime el dia en minuscula 
    //echo date("D" );//imprime el dia con 3 letras 
    //echo date("d" );//imprime el mes 2 diigitos de 0 en adelante
    //echo date("F" );//imprime el mes textual completo
    //echo date("h" );//imprime formato de una hora con ceros iniciales
    //echo date("i" );//imprime minutos con ceros iniciales
    //echo date("a" );//imprime am o pm en minusculas
    //echo date(" m ");//imprime el mes en numero (M para mayuscula)
    //echo date(" Y ");

    echo ("The current date is: " );
    echo date("D, d F Y h:i a " );
